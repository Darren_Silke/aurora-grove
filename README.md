# Aurora Grove

Aurora Grove is a simple one-page responsive website for a fictitious camping resort. It was built using [Bootstrap](https://getbootstrap.com) in conjunction with HTML and [SASS](https://sass-lang.com).

A live version of Aurora Grove can be viewed [here](https://auroragrovecamping.netlify.app).

## Running Locally

Clone the repository and open the 'index.html' file in your Web browser.
